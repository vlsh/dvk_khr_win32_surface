#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(dead_code)]

#[macro_use]
extern crate bitflags;
extern crate libc;
extern crate shared_library;
extern crate winapi;
#[macro_use]
extern crate dvk;
extern crate dvk_khr_surface;

use self::libc::{c_void, c_char, uint32_t, size_t, uint64_t, c_float, int32_t, uint8_t};
use self::shared_library::dynamic_library::DynamicLibrary;
use std::path::{Path};
use std::ffi::CString;
use winapi::{HINSTANCE, HWND};
use dvk::*;
use dvk_khr_surface::*;

pub const VK_KHR_WIN32_SURFACE_SPEC_VERSION: uint32_t = 5;
pub const VK_KHR_WIN32_SURFACE_EXTENSION_NAME: *const c_char = b"VK_KHR_win32_surface\0" as *const u8 as *const c_char;

#[repr(u32)]
#[derive(Eq)]
#[derive(PartialEq)]
#[derive(Debug)]
pub enum VkKhrSurfaceStructureType {
    VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR = 1000009000
}

pub type VkWin32SurfaceCreateFlagsKHR = VkFlags;

#[repr(C)]
pub struct VkWin32SurfaceCreateInfoKHR {
    pub sType: VkKhrSurfaceStructureType,
    pub pNext: *const c_void,
    pub flags: VkWin32SurfaceCreateFlagsKHR,
    pub hinstance: HINSTANCE,
    pub hwnd: HWND
}

pub type vkCreateWin32SurfaceKHRFn = unsafe extern "stdcall" fn(instance: VkInstance, 
                                                                pCreateInfo: *const VkWin32SurfaceCreateInfoKHR,
                                                                pAllocator: *const VkAllocationCallbacks,
                                                                pSurface: *mut VkSurfaceKHR) -> VkResult;

pub type vkGetPhysicalDeviceWin32PresentationSupportKHRFn = unsafe extern "stdcall" fn(physicalDevice: VkPhysicalDevice, 
                                                                                       queueFamilyIndex: uint32_t) -> VkBool32;

#[cfg(windows)]
static VULKAN_LIBRARY: &'static str = "vulkan-1.dll";

#[cfg(unix)]
static VULKAN_LIBRARY: &'static str = "libvulkan-1.so";

#[derive(Default)]
pub struct VulkanKhrWin32Surface {
   library: Option<DynamicLibrary>,
   vkGetInstanceProcAddr: Option<vkGetInstanceProcAddrFn>,
   vkCreateWin32SurfaceKHR: Option<vkCreateWin32SurfaceKHRFn>,
   vkGetPhysicalDeviceWin32PresentationSupportKHR: Option<vkGetPhysicalDeviceWin32PresentationSupportKHRFn>
}

impl VulkanKhrWin32Surface {
    pub fn new() -> Result<VulkanKhrWin32Surface, String> {
        let mut vulkan_khr_win32_surface: VulkanKhrWin32Surface = Default::default();
        let library_path = Path::new(VULKAN_LIBRARY);
        vulkan_khr_win32_surface.library = match DynamicLibrary::open(Some(library_path)) {
            Err(error) => return Err(format!("Failed to load {}: {}",VULKAN_LIBRARY,error)),
            Ok(library) => Some(library),
        };
        unsafe {
            vulkan_khr_win32_surface.vkGetInstanceProcAddr = Some(std::mem::transmute(try!(vulkan_khr_win32_surface.library.as_ref().unwrap().symbol::<u8>("vkGetInstanceProcAddr"))));
        }
        Ok(vulkan_khr_win32_surface)
    }

    unsafe fn load_command(&self, instance: VkInstance, name: &str) -> Result<vkVoidFunctionFn, String> {
        let fn_ptr = (self.vkGetInstanceProcAddr.as_ref().unwrap())(instance, CString::new(name).unwrap().as_ptr());
        if fn_ptr != std::ptr::null() {
            Ok(fn_ptr)
        } else {
            Err(format!("Failed to load {}",name))
        }
    }

    pub fn load(&mut self, instance: VkInstance) -> Result<(), String> {
        unsafe {
            self.vkCreateWin32SurfaceKHR = Some(std::mem::transmute(try!(self.load_command(instance, "vkCreateWin32SurfaceKHR"))));
            self.vkGetPhysicalDeviceWin32PresentationSupportKHR = Some(std::mem::transmute(try!(self.load_command(instance, "vkGetPhysicalDeviceWin32PresentationSupportKHR"))));
        }
        Ok(())
    }

    pub unsafe fn vkCreateWin32SurfaceKHR(&self,
                                          instance: VkInstance,
                                          pCreateInfo: *const VkWin32SurfaceCreateInfoKHR,
                                          pAllocator: *const VkAllocationCallbacks,
                                          pSurface: *mut VkSurfaceKHR) -> VkResult {
        (self.vkCreateWin32SurfaceKHR.as_ref().unwrap())(instance, pCreateInfo, pAllocator, pSurface)
    }

    pub unsafe fn vkGetPhysicalDeviceWin32PresentationSupportKHR(&self,
                                                                 physicalDevice: VkPhysicalDevice,
                                                                 queueFamilyIndex: uint32_t) -> VkBool32 {
        (self.vkGetPhysicalDeviceWin32PresentationSupportKHR.as_ref().unwrap())(physicalDevice, queueFamilyIndex)
    }
}

